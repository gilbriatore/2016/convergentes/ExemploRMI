package br.edu.up;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

public class CalculadoraImpl extends UnicastRemoteObject implements Calculadora {

	private static final long serialVersionUID = 8323213732544464473L;

	protected CalculadoraImpl() throws RemoteException {
		super();
	}

	@Override
	public int somar(int a, int b) throws RemoteException {
		return a + b;
	}

}
