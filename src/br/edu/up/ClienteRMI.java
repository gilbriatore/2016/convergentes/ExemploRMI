package br.edu.up;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;

public class ClienteRMI {
	
	
	public static void main(String[] args) throws MalformedURLException, RemoteException, NotBoundException {
	
		Calculadora c = (Calculadora) Naming.lookup("rmi://localhost:9977/calcRmi"); 
		
		//Chamada de procedimento remoto
		int resultado = c.somar(10, 5);
		
		System.out.println("Resultado: " + resultado); 
		
		
		
	}
}
