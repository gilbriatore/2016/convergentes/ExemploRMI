package br.edu.up;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface Calculadora extends Remote {
	
	public int somar(int a, int b) throws RemoteException;
}
